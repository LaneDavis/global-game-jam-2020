﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableTims : MonoBehaviour {
    public List<GameObject> enableSpawns;

    // Start is called before the first frame update
    void Start() { }

    private void OnTriggerEnter2D(Collider2D other) {

        if (other.tag == "Player") {
            foreach (GameObject timsSpawn in enableSpawns) {
                timsSpawn.SetActive(true);
            }
        }

    }




    // Update is called once per frame
        void Update() { }
    
}