﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class FxDetachOnTrigger : MonoBehaviour {

    public bool DestroySelf;
    [ShowIf("DestroySelf")] public float DestroyTime;

    private float detachTime;
    private bool isDetached;
    
    private FxController controller;
    
    private void Start() {
        controller = GetComponent<FxController>();
        if (controller == null) {
            Debug.LogWarning("No FxController set for FxDetachOnTrigger");
        }
        controller.OnTrigger += Detach;
    }

    private void Detach() {
        isDetached = true;
        detachTime = Time.time;
        transform.SetParent(null);
    }

    private void Update() {
        if (isDetached && DestroySelf && Time.time > detachTime + DestroyTime) {
            Destroy(gameObject);
        }
    }
    
}
