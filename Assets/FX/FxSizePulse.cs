﻿using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class FxSizePulse : FxVariableValue {
	
	[Header("Wave Attributes")]
	public AnimationCurve EaseInCurve = new AnimationCurve (new Keyframe (0F, 0F, 4F, 4F), new Keyframe (0.45F, 1F, 0F, 0F));
	[Range(0F, 10F)] public float WaveFrequency = 1F;

	[Header("Inversion (Squash & Stretch)")]
	public bool InvertX;
	
	//ANATOMY
	private FxApplierSizePulse applier;

	public override void SetUp (GameObject newAnchor = null) {

		base.SetUp(newAnchor);
		if (newAnchor == null) return;
		Anchor = newAnchor;
 
		if (newAnchor.GetComponent<FxApplierSizePulse>() != null) {
			applier = newAnchor.GetComponent<FxApplierSizePulse>();
		} else {
			applier = newAnchor.AddComponent<FxApplierSizePulse>();
		}
		applier.SetUp(newAnchor);

	}
	
	public override void Trigger (GameObject newAnchor = null, float power = 1f) {
		base.Trigger(newAnchor);
		applier.SetPulseFactor(SfxId, MyValueFactor(power), EaseInCurve, WaveFrequency, InvertX);
	}
	
	public override void Toggle (bool toggleState, GameObject newAnchor = null, float power = 1f) {
		base.Toggle(toggleState, newAnchor);
		if (toggleState) 
			applier.SetPulseFactor(SfxId, MyValueFactor(power), EaseInCurve, WaveFrequency, InvertX);
		else 
			applier.EndFactor(SfxId);
	}
	
}

#if UNITY_EDITOR
[CanEditMultipleObjects]
[CustomEditor(typeof(FxSizePulse))]
public class FxSizePulseEditor : Editor {
	override public void OnInspectorGUI() {
        
		var myScript = target as FxSizePulse;
        
		List<string> excludedProperties = new List<string>();
		serializedObject.Update();

		if(myScript.Style != ValueFactor.FactorStyle.Timed){
			excludedProperties.Add("Timed_Duration");
		}
        
		if(myScript.Style != ValueFactor.FactorStyle.Decay){
			excludedProperties.Add("Decay_DecayRate");
			excludedProperties.Add("Decay_MaxDuration");
		}
        
		if(myScript.Style != ValueFactor.FactorStyle.Curve){
			excludedProperties.Add("Curve_Curve");
		}
		else {
			excludedProperties.Add("BaseValue");
		}
        
		if(myScript.Style != ValueFactor.FactorStyle.Toggle){
			excludedProperties.Add("Toggle_Easing");
		}

		if (myScript.Style != ValueFactor.FactorStyle.Toggle || !myScript.Toggle_Easing) {
			excludedProperties.Add("Toggle_EaseIn");
			excludedProperties.Add("Toggle_EaseOut");
		}

		DrawPropertiesExcluding(serializedObject, excludedProperties.ToArray());
		serializedObject.ApplyModifiedProperties();

	}
}
#endif
