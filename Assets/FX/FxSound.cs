﻿

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class FxSound  : Fx {

    public SoundCall instantSound;
    public SoundCall durationLoop;
    protected float baseDurationVolume = 0F;

    public AnimationCurve durationCurve = AnimationCurve.EaseInOut(0F, 0F, 1F, 1F);
    public float durationDecaySpeed = 5F;

    //State: Time
    protected float durationTimer;
    //State: Strength
    protected float curDurationMultiplier;

    void Start () {
        baseDurationVolume = durationLoop.volume;
        durationLoop.volume = 0F;
    }

    public override void Trigger (GameObject newAnchor = null, float power = 1f) {
        SoundManager.Instance.PlaySound(instantSound, gameObject);
    }

    public override void Toggle (bool toggleState, GameObject newAnchor = null, float power = 1f) {
        if (toggleState) {
            durationCurve.keys[0].value = curDurationMultiplier;
            durationTimer = 0F;
        }
    }

    void Update () {

        if (ToggleState) {
            durationTimer += Time.deltaTime;
            curDurationMultiplier = durationCurve.Evaluate(durationTimer);
        }
        else {
            curDurationMultiplier = MathUtilities.Decay(curDurationMultiplier, durationDecaySpeed, Time.deltaTime);
        }

        durationLoop.volume = baseDurationVolume * curDurationMultiplier;
        if (curDurationMultiplier > 0.05F) {
            SoundManager.Instance.PlaySound(durationLoop, gameObject);
        }

    }

}

#if UNITY_EDITOR
[CustomEditor(typeof(FxSound))]
public class FxSoundEditor : Editor {


    FxSound soundScript;

    public override void OnInspectorGUI () {
        base.OnInspectorGUI();
        soundScript = (FxSound)target;
        if (GUILayout.Button("Null Loaded Sounds")) {
            NullSound(soundScript.instantSound);
            NullSound(soundScript.durationLoop);
        }
    }

    void NullSound (SoundCall sound) {
        sound.soundKey = "";
        sound.soundUsed = null;
        sound.priority = 0;
        sound.volume = 0;
        sound.pitch = 0;
        sound.pitchVariation = 0;
        sound.autoFadeRate = 0;
        sound.autoFades = false;
    }
}
#endif
