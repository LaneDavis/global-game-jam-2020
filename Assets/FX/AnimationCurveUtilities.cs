﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class AnimationCurveUtilities {

    public static AnimationCurve MultipliedCurve(AnimationCurve inputCurve, float amplitude) {
        if (amplitude == 1f) return inputCurve;
        Keyframe[] frames = new Keyframe[inputCurve.keys.Length];
        for (int i = 0; i < inputCurve.keys.Length; i++) {
            frames[i] = new Keyframe(inputCurve.keys[i].time, inputCurve.keys[i].value * amplitude, inputCurve.keys[i].inTangent * amplitude, inputCurve.keys[i].outTangent * amplitude);
        }
        return new AnimationCurve(frames);
    }
    
    public static Keyframe LastFrame(this AnimationCurve curve) {
        return curve.keys[curve.keys.Length - 1];
    }

    public static Keyframe HighFrame(this AnimationCurve curve) {
        return curve.keys.OrderBy(e => e.value).Last();
    }

    public static Keyframe LowFrame(this AnimationCurve curve) {
        return curve.keys.OrderBy(e => e.value).First();
    }
    
}
