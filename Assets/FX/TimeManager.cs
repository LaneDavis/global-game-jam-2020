﻿using System.Collections.Generic;
using System.Linq;
using Rewired;
using UnityEngine;

public class TimeManager : MonoBehaviour {

    private static TimeManager Instance;
    public FxController PauseFactor;

    private List<ValueFactor> factors = new List<ValueFactor>();
    
    private void Awake() {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    public static void AddFactor(ValueFactor newFactor) {
        if (newFactor.ID != -1) {
            if (Instance.GetFactorWithID(newFactor.ID) != null) {
                return;
            }
        }
        Instance.factors.Add(newFactor);
    }

    public static void RemoveFactor(int ID) {
        ValueFactor factor = Instance.GetFactorWithID(ID);
        if (factor != null) {
            Instance.factors.Remove(factor);
        }
    }

    private void Update() {
        
        // Calculate Timescale
        float maxSlowdown = 0f;
        if (factors.Count != 0) maxSlowdown = factors.Max(e => e.Value);
        
        // Apply Timescale
        float timeRatioThisFrame = (1f - maxSlowdown) / Time.timeScale;
        Time.timeScale = Mathf.Max(0, 1f - maxSlowdown);
        
        // Age Time Factors
        for (int i = factors.Count - 1; i >= 0; i--) {
            
            // If the factor pauses the game, update it using unscaled delta time.
            if (factors[i].Value >= 1f) { factors[i].UpdateAge(unscaled:true); }
            
            // If the factor does not pause the game, age it as follows:
            // Multiply in (1 / factor speed) to capture that the factor does not get slowed down at all at its own target speed.
            // Make it NOT use unscaled time, since it is affected by timescales that are lower than itself.
            
            // Examples:
            // A value of 0.6 causes a slowdown of 60%, for a Time.timeScale of 0.4.
            // This results in a timeScaleMultiplier of 2.5, which results in this factor being effectively unscaled.
            // If there were a second factor with a value of 0.2, it will have a timeScaleMultiplier of 1/0.8 or 1.25.
            // The second factor would therefore last longer and may come into effect when the first one disappears.
            // Meanwhile, factors other than pausing factors will not age at all while pausing factors are in effect.
            else { factors[i].UpdateAge(1f / (1f - factors[i].Value) * timeRatioThisFrame); }
            if (factors[i].ShouldBeDeleted) factors.RemoveAt(i);
            
        }

    }

    private ValueFactor GetFactorWithID(int ID) {
        foreach (ValueFactor factor in factors) {
            if (factor.ID == ID) return factor;
        }

        return null;
    }

    public static void PauseGame(bool isPaused) {
        foreach(ValueFactor test in Instance.factors)
        {
            Debug.Log(test);
        }
        Instance.PauseFactor.Toggle(isPaused);
    }

}
