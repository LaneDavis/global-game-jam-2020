﻿// MyCharacter.cs - A simple example showing how to get input from Rewired.Player

using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine.SceneManagement;


public class DotController : MonoBehaviour {

    public static int PlayerCount = 1;
    
    // The Rewired player id of this character
    public int playerId = 0;
    private bool isOther;
    
    // The movement speed of this character
    public float moveSpeed = 3.0f;

    public int maxHP = 20;
    private int currentHP = 1;

    // Are you the virus? lmao
    public bool isVirus;
    // Your other teammate lol
    public DotController teamMate;

    public AnimationCurve ScaleMagnitudeByHPPercent;
    public float RescaleTime;
    private float rescaleCur = 1f;
    private float rescaleVel;

    public GameObject PowerupPrefab;

    private Player player; // The Rewired Player
    public Vector3 moveVector;
    private bool fire;

    public AnimationCurve ShoveCurve;
    
    public delegate void ActionDelegate();
    public event ActionDelegate OnActionPressed;
    public event ActionDelegate OnActionHeld;
    public event ActionDelegate OnActionReleased;

    public delegate void CollectPowerupDelegate();
    public event CollectPowerupDelegate OnCollectPowerup;

    public FxController CollectFX;

    private List<IMoveInfluence> MoveInfluences = new List<IMoveInfluence>();

    public SoundCall PowerupCollectionSound;

    public List<GameObject> art;

    [Header("Near Dead Pulse")]
    public List<FxController> NearDeadPulseFX = new List<FxController>();
    public float NearDeadPulseCooldown = 0.6f;
    private float timeOfNextNearDeadPulse;

    void Awake() {
        if (PlayerCount == 1 && playerId != 0) {
            isOther = true;
            playerId = 0;
        }
        currentHP = maxHP;
        transform.position = WinZone.activeWinZone;
        // Get the Rewired Player object for this player and keep it for the duration of the character's lifetime
        player = ReInput.players.GetPlayer(playerId);
        foreach (IPlayerRecipient playerRecipient in GetComponentsInChildren<IPlayerRecipient>()) {
            playerRecipient.SetRewiredPlayer(player);
        }

    }

    void Start() {
        if (CameraController.Instance != null) {
            CameraController.Instance.AddTrackedTransform(transform);
        }

        foreach (IMoveInfluence moveInfluence in GetComponentsInChildren<IMoveInfluence>()) {
            MoveInfluences.Add(moveInfluence);
        }
    }

    void Update () {
        GetInput();
        ProcessInput();
        SetSize();

        if (currentHP <= 1 && Time.time > timeOfNextNearDeadPulse) {
            DoNearDeadPulse();
        }
    }

    private void GetInput() {
        // Get the input from the Rewired Player. All controllers that the Player owns will contribute, so it doesn't matter
        // whether the input is coming from a joystick, the keyboard, mouse, or a custom controller.

        if (!isOther) {
            moveVector.x = player.GetAxis("MoveX"); // get input by name or action id
            moveVector.y = player.GetAxis("MoveY");
        }
        else {
            moveVector.x = player.GetAxis("OtherMoveX");
            moveVector.y = player.GetAxis("OtherMoveY");
        }

        if (player.GetButtonDown("Action")) OnActionPressed?.Invoke();
        if (player.GetButton("Action")) OnActionHeld?.Invoke();
        if (player.GetButtonUp("Action")) OnActionReleased?.Invoke();
        
    }

    private void ProcessInput() {
        // Process movement
        if(moveVector.x != 0.0f || moveVector.y != 0.0f) {
            float moveInfluenceSpeed = 1f;
            foreach (IMoveInfluence moveInfluence in MoveInfluences) {
                moveInfluenceSpeed *= moveInfluence.GetMoveMultiplier();
            }
            transform.Translate(moveVector * moveSpeed * moveInfluenceSpeed * Time.deltaTime);
        }
    }

    // COLLISIONS
    private void OnTriggerEnter2D(Collider2D other) { ProcessCollisionOrTrigger2D(other); }
    private void OnCollisionEnter2D(Collision2D other) { ProcessCollisionOrTrigger2D(other.gameObject.GetComponent<Collider2D>()); }
    private void OnTriggerStay2D(Collider2D other) { ProcessCollisionOrTrigger2D(other); }

    private void ProcessCollisionOrTrigger2D(Collider2D other) {
        if (other.CompareTag("Player")) {
            ShoveAwayFromFriend(other);
        }
        else if (other.CompareTag("Powerup") && other.name.Contains(PowerupPrefab.name)) {
            SoundManager.Instance.PlaySound(PowerupCollectionSound, gameObject);
            if (CollectFX != null) CollectFX.Trigger();
            OnCollectPowerup?.Invoke();
            other.gameObject.SetActive(false);
            currentHP = maxHP;
        }
        else if (other.CompareTag("Enemy")){
            TakeImpactFromEnemy();
        } else if (other.CompareTag("Heart")) {
            if (isVirus) {
                other.SendMessage("ExplodeHeart");
                foreach(GameObject artPart in art) {
                    if (artPart)
                        Destroy(artPart.gameObject);
                }
                moveSpeed = 0;
                if (teamMate)
                    Destroy(teamMate.gameObject);
            }
        }
    }

    private void TakeImpactFromEnemy() {
        currentHP = currentHP - 1;
        if (currentHP == 0) {
            PlayerStats.IncrementDeaths();
            SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
        }
    }

    private void ShoveAwayFromFriend(Collider2D friendCollider) {
        Vector2 myPosition = transform.position;
        Vector2 theirPosition = friendCollider.transform.position;
        float myRadius = GetComponent<CircleCollider2D>().radius * transform.localScale.x;
        float theirRadius = friendCollider.GetComponent<CircleCollider2D>().radius * friendCollider.transform.localScale.x;

        Vector2 difference = myPosition - theirPosition;
        Vector2 direction = difference.normalized;
        float distance = myRadius + theirRadius;

        Vector2 desiredPlace = theirPosition + direction * distance;
        Vector2 differenceToDesiredPlace = desiredPlace - myPosition;
        Vector2 dirToDesired = differenceToDesiredPlace.normalized;
        float distToDesired = differenceToDesiredPlace.magnitude;
        
        transform.Translate((desiredPlace - (Vector2)transform.position) * ShoveCurve.Evaluate(distToDesired) * Time.deltaTime);
    }

    private void SetSize() {
        float desiredSize = ScaleMagnitudeByHPPercent.Evaluate(currentHP / (float) maxHP);
        rescaleCur = Mathf.SmoothDamp(rescaleCur, desiredSize, ref rescaleVel, RescaleTime);
        transform.localScale = Vector3.one * rescaleCur;
    }

    private void DoNearDeadPulse() {
        timeOfNextNearDeadPulse = Time.time + NearDeadPulseCooldown;
        foreach (FxController fx in NearDeadPulseFX) {
            fx.Trigger();
        }
    }
    
}