﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkController : MonoBehaviour {
    
    public AnimationCurve YCurve;
    private float blinkTimer;
    private float timeOfNextBlink;
    private float timeOfLastBlink = -100f;

    public float MaxTimeBetweenBlinks;
    public float MinTimeBetweenBlinks;

    private Vector3 baseScale;

    private void Start() {
        baseScale = transform.localScale;
        timeOfNextBlink = Time.time + Random.Range(MinTimeBetweenBlinks, MaxTimeBetweenBlinks);
    }

    private void Update() {
        transform.localScale = new Vector3(baseScale.x, baseScale.y * YCurve.Evaluate(Time.time - timeOfLastBlink), baseScale.z);
        if (Time.time > timeOfNextBlink) {
            Blink();
        }
    }

    private void Blink() {
        timeOfLastBlink = Time.time;
        timeOfNextBlink = Time.time + Random.Range(MinTimeBetweenBlinks, MaxTimeBetweenBlinks);
    }

}
