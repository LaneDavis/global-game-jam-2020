﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodgerController : EnemyController
{
  private Collider2D collider2d;         //The BoxCollider2D component attached to this object.
  public float rotationRange = 30;
  public float maxDistanceFromCamera = 15.0f;
  private Camera mainCamera;
  protected override void Start()
  {
    base.Start();
    //Get a component reference to this object's BoxCollider2D
    collider2d = GetComponent<Collider2D>();
    mainCamera = Camera.main;
    //transform.eulerAngles = Vector3.forward * (Random.Range(-(rotationRange / 2), (rotationRange / 2)));
    transform.Rotate(0f, 0f, (Random.Range(-(rotationRange / 2), (rotationRange / 2))));
    // or
    // transform.rotation = Quaternion.Euler(Vector3.forward * degrees);
    // or
    // transform.rotation = Quaternion.LookRotation(Vector3.forward, yAxisDirection);
    // or
    // transform.LookAt(Vector3.forward, yAxisDirection);
    // or
    // transform.right = xAxisDirection;
    // or
    // transform.up = yAxisDirection;
  }

  public float speed = 2.0f;

  // Update is called once per frame
  protected override void Update()
  { 
    base.Update();
    transform.position += -transform.right * speed * Time.deltaTime * moveSpeedMultiplier;
    if (mainCamera != null)
    {
      if (Vector2.Distance(gameObject.transform.position, mainCamera.transform.position) > maxDistanceFromCamera)
      {
        Destroy(gameObject);
      }
      // CameraController.Instance.transform.position;s
    }
  }

  private void OnTriggerEnter2D(Collider2D other)
  {
    Debug.Log("All your base are belong to us");
  }
}
