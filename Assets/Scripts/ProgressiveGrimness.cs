﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressiveGrimness : MonoBehaviour {

    public AnimationCurve GrimnessByX;
    public float GrimnessSineSpeed;
    public float GrimnessSineAmplitude;
    private float timer;

    private void Update() {
        timer += Time.deltaTime;
        float grimness = GrimnessByX.Evaluate(CameraController.Instance.transform.position.x)  + Mathf.Sin(timer * GrimnessSineSpeed) * GrimnessSineAmplitude;
        PPManager.Instance.AddFactor(PPManager.Volume.Grim, new ValueFactor(grimness, 0f, 10000f), -1);
    }

}
