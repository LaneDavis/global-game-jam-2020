﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Extensions {
    
    public static Vector3 FromMag (this Vector3 v, float magnitude) {
        return new Vector3(magnitude, magnitude, magnitude);
    }
    
}
