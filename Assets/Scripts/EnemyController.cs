﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public delegate void DeathDelegate();
    public event DeathDelegate OnBeforeDeath;
    public float moveSpeedMultiplier = 1.0f;

    public AnimationCurve ScaleCurve;
    private float timer;

    public SoundCall BirthSound;

    public GameObject DeathFXPrefab;
    
    protected virtual void Start() {
        EnemyManager.Instance.RegisterEnemy(this);
        if (SoundManager.Instance != null) {
            SoundManager.Instance.PlaySound(BirthSound, gameObject);
        }
    }

    public void BeforeDeath() {
        OnBeforeDeath?.Invoke();
    }
    
    public virtual void ReceiveVirusBlast() {
        BeforeDeath();
        PlayerStats.IncrementKills();
        Destroy(gameObject);
    }

    protected virtual void Update() {
        timer += Time.deltaTime;
        transform.localScale = Vector3.one * ScaleCurve.Evaluate(timer);
    }

    private void OnDestroy() {
        if (DeathFXPrefab != null) Instantiate(DeathFXPrefab, transform.position, Quaternion.identity, null);
        EnemyManager.Instance.UnregisterEnemy(this);
    }
    
}
