﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteBloodCellController : EnemyController
{
  // Start is called before the first frame update
  private Collider2D collider2d;         //The BoxCollider2D component attached to this object.
  public float maxSpeed = 5.0f;
  public float minSpeed = 0.5f;
  public float maxDistanceFromCamera = 15.0f;
  private Camera mainCamera;
  private float randomSpeed;
  protected override void Start()
  {
    base.Start();
    
    //Get a component reference to this object's BoxCollider2D
    collider2d = GetComponent<Collider2D>();
    randomSpeed = Random.Range(minSpeed, maxSpeed);
    mainCamera = Camera.main;

  }

  // public float speed = 2.0f;


  // Update is called once per frame
  protected override void Update()
  { 
    base.Update();
    transform.position += -transform.right * randomSpeed * Time.deltaTime * moveSpeedMultiplier;
    if (mainCamera != null)
    {
      if (Vector2.Distance(gameObject.transform.position, mainCamera.transform.position) > maxDistanceFromCamera)
      {
        Destroy(gameObject);
      }
      // CameraController.Instance.transform.position;s
    }
  }

  private void OnTriggerEnter2D(Collider2D other)
  {
    if (other.tag == "Player") {
      Debug.Log("let the bodies hit the floor");
      BeforeDeath();
      Destroy(gameObject);
    }

    


  }
  private void OnBecameInvisible()
  {
    Destroy(gameObject);
  }

}
