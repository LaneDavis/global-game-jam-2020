using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSelectScript : MonoBehaviour {
    // Start is called before the first frame update
    
	public void selectScene(){
		switch (this.gameObject.name) {
		case "Solo":
			SceneManager.LoadScene ("Level 1");
			DotController.PlayerCount = 1;
			break;
		case "Multiplayer":
			SceneManager.LoadScene ("Level 1");
			DotController.PlayerCount = 2;
			break;
		case "Credits":
			SceneManager.LoadScene ("SQQQUUUUUUUUUUUUUAD");
			break;
		case "Quit":
			Application.Quit();
			break;
		case "Back":
			SceneManager.LoadScene ("MainMuzel");
			break;
    	}   
    }
}
