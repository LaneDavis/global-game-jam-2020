﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ChaserMovement : EnemyController
{
  private GameObject lilBoy;
  public float speed = 1.0f;
  public float intervalTime = 1.0f;
  public float maxDistanceFromCamera = 15.0f;
  private bool goTime = false;
  private Camera mainCamera;

  //   var heading = target.position - player.position;

  // Start is called before the first frame update
  protected override void Start()
  {
    base.Start();
    foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
    {
      if (player.name == "Lil Boi")
      {
        lilBoy = player;
        //Do Something
      }
    }
    InvokeRepeating("ToggleGoTime", 0, intervalTime);
    mainCamera = Camera.main;
    // GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
  }
  private void ToggleGoTime()
  {
    goTime = !goTime;
  }
  // Update is called once per frame
  protected override void Update()
  { 
    base.Update();
    if (lilBoy == null)
        return;
    if (gameObject.transform.position.x > lilBoy.transform.position.x)
    {
      if (goTime)
      {
        transform.position += (lilBoy.transform.position - gameObject.transform.position).normalized * speed * Time.deltaTime * moveSpeedMultiplier;
      }
    }
    else
    {
      transform.position += -transform.right * speed * 5 * Time.deltaTime;
    }
    if (mainCamera != null)
    {
      if (Vector2.Distance(gameObject.transform.position, mainCamera.transform.position) > maxDistanceFromCamera)
      {
        Destroy(gameObject);
      }

    }
  }
  private void OnTriggerEnter2D(Collider2D other) {
    BeforeDeath();
    Destroy(gameObject);
  }
}
