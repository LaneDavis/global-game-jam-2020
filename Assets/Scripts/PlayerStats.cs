﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;

public class PlayerStats : MonoBehaviour {

    public static PlayerStats Instance;

    public TextMeshProUGUI TimeText;
    public TextMeshProUGUI DeathsText;
    public FxController DeathsFX;
    public TextMeshProUGUI KillsText;
    public FxController KillsFX;

    private static int Deaths;
    private static int Kills;

    private static float StartTime;
    
    private void Awake() {
        Instance = this;
        RefreshDeathsText(false);
        RefreshKillsText(false);
    }

    private void OnEnable() {
        StartTime = Time.time;
    }

    public static void IncrementDeaths() {
        Deaths++;
        RefreshDeathsText();
    }

    public static void IncrementKills() {
        Kills++;
        RefreshKillsText();
    }

    private static void RefreshDeathsText(bool doFx = false) {
        if (Instance == null) return;
        Instance.DeathsText.text = $"Deaths: {Deaths}";
        if (!doFx) return;
        Instance.DeathsFX.Trigger();
    }
    
    private static void RefreshKillsText(bool doFx = false) {
        if (Instance == null) return;
        Instance.KillsText.text = $"Kills: {Kills}";
        if (!doFx) return;
        Instance.KillsFX.Trigger();
    }

    private void Update() {
        TimeText.text = TimeTextFromMilliseconds((int) ((Time.time - StartTime) * 1000));
    }
    
    /// <summary>
    /// A different way to format text, this time for a digital timer that goes down to the millisecond.
    /// </summary>
    /// <param name="milliseconds">The number of milliseconds being counted.</param>
    /// <returns>A formatted string.</returns>
    public static string TimeTextFromMilliseconds(int milliseconds) {
        int ms = milliseconds % 1000;
        int seconds = (milliseconds / 1000) % 60;
        int minutes = (milliseconds / 60000) % 60;
        int hours = (milliseconds / 3600000);
        StringBuilder sb = new StringBuilder(13);
        if (hours > 0) {
            sb.AppendFormat("<mspace=0.7em>{0:D2}</mspace>:", hours);
        }

        sb.AppendFormat("<mspace=0.7em>{0:D2}</mspace>:<mspace=0.7em>{1:D2}</mspace>.<mspace=0.7em>{2:D3}</mspace>", minutes, seconds, ms);
        return sb.ToString();
    }
    
}
