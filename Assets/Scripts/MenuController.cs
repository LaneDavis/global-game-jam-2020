﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class MenuController : MonoBehaviour
{
	public GameObject OffCanvas;
	public GameObject OnCanvas;
	public GameObject FirstObject;
	public void Switch()
    {
       OffCanvas.SetActive (true);
       OnCanvas.SetActive (false);
       GameObject.Find ("EventSystems").GetComponent<EventSystem> ().SetSelectedGameObject (FirstObject, null); 
    }
}
    