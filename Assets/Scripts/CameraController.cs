﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public static CameraController Instance;
    public Camera Camera;
    private List<Transform> trackedObjects = new List<Transform>();

    public float PaddingAdd;
    public float PaddingMultiply;

    public AnimationCurve RepositionCurve;
    public float ResizeTime;
    private float resizeVel;

    public FxApplierTranslate ShakeTranslator;
    public FxApplierRotate ShakeRotator;
    public float ShakeTranslateMax;
    public float ShakeRotateMax;
    public float ShakeDecayRate;
    public float ShakePerlinSpeed;

    public float TestShakeAmount = 1f;
    private float shakeTrauma;

    private void Awake() {
        Instance = this;
        Vector3 pos = WinZone.activeWinZone;
        pos.z = -10f;
        transform.position = pos;
    }

    private void Start() {
        ShakeRotator.SetUp(Camera.gameObject);
        ShakeTranslator.SetUp(Camera.gameObject);
    }

    public void AddTrackedTransform(Transform newTransform) {
        if (!trackedObjects.Contains(newTransform)) {
            trackedObjects.Add(newTransform);
        }
    }

    public void ApplyShake(float shake) {
        shakeTrauma = Mathf.Clamp01(Mathf.Max(shakeTrauma, shake));
    }

    [Button("Test Shake")]
    public void TestShake() {
        ApplyShake(TestShakeAmount);
    }
    
    private void Update() {

        ShakeRotator.SetShakeFactor(-1, new ValueFactor(shakeTrauma * ShakeRotateMax, ShakeDecayRate, 10000f), ShakePerlinSpeed);
        ShakeTranslator.SetShakeFactor(-1, new ValueFactor(shakeTrauma * ShakeTranslateMax, ShakeDecayRate, 10000f), ShakePerlinSpeed);
        shakeTrauma = MathUtilities.Decay(shakeTrauma, Time.deltaTime, ShakeDecayRate);
        if (trackedObjects.Count > 0 && trackedObjects.Where(e => e != null).ToList().Count > 0) {
            UpdateObjectTracking();
        }
       
    }

    private void UpdateObjectTracking() {
        foreach (Object e in trackedObjects) {
            if (e == null)
                return;
        }
        float xMax = trackedObjects.Where(e => e != null).Max(e => e.position.x);
        float xMin = trackedObjects.Where(e => e != null).Min(e => e.position.x);
        float yMax = trackedObjects.Where(e => e != null).Max(e => e.position.y);
        float yMin = trackedObjects.Where(e => e != null).Min(e => e.position.y);
        Vector2 targetPos = new Vector2((xMax + xMin) / 2f, (yMax + yMin) / 2f);
        float desiredWidth = (xMax - xMin) * PaddingMultiply + PaddingAdd;
        float desiredHeight = (yMax - yMin) * PaddingMultiply + PaddingAdd;
        
        // Move
        Vector2 posDiff = targetPos - (Vector2) transform.position;
        Vector2 posDir = posDiff.normalized;
        float posMag = posDiff.magnitude;
        transform.Translate(posDir * RepositionCurve.Evaluate(posMag) * Time.deltaTime);
        transform.position = new Vector3(transform.position.x, transform.position.y, -10);
        
        // Resize
        float widthOverHeight = Screen.width / Screen.height;
        float curHeight = Camera.orthographicSize * 2f;
        float curWidth = curHeight * widthOverHeight;
        bool keyAxisIsWidth = desiredWidth > desiredHeight * widthOverHeight;
        float desiredOrthographicSize;
        if (keyAxisIsWidth) {
            desiredOrthographicSize = desiredWidth / widthOverHeight / 2;
        }
        else {
            desiredOrthographicSize = desiredHeight / 2;
        }

        Camera.orthographicSize = Mathf.SmoothDamp(Camera.orthographicSize, desiredOrthographicSize, ref resizeVel, ResizeTime);
    }

}
