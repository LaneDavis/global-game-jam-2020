﻿using System.Collections.Generic;
using System.Diagnostics;
using Rewired;
using UnityEngine;

public class SmallVirusBurstAction : MonoBehaviour, IMoveInfluence, IPlayerRecipient {

    private Player rewiredPlayer;
    
    public DotController Controller;
    public AnimationCurve MoveMultiplierByChargeTime;
    public AnimationCurve RingSizeByChargeTime;
    public AnimationCurve RingRandomnessByChargeTime;
    public AnimationCurve ChargeUpFXStrengthByChargeTime;
    public AnimationCurve BurstFXStrengthByChargeTime;

    public SpriteRenderer RingSpriteRenderer;
    public Gradient RingGradient;
    public AnimationCurve GradientSpeedByChargeTime;
    private float ringGradientTimer;
    
    public Transform RingTransform;

    public float TimerReleaseThreshold;

    public FxController ChargeUpFX;
    public FxController BurstFX;
    
    private bool isChargingUp;
    private float chargeTimer;
    private bool hasPowerup;

    [Header("Sounds")]
    public SoundCall ChargeLoop;
    public AnimationCurve LoopVolumeByChargeTime;
    
    [Header("Lightning Particles")]
    public ParticleSystem FrontParticles;
    public ParticleSystem BackParticles;
    public float FrontParticlesEmitRate;
    public float BackParticlesEmitRate;
    public float OffSimulationSpeed = 5f;
    public float OnSimulationSpeed = 1.25f;
    public float SimulationSpeedTransitionTime = 0.6f;
    private float simulationSpeedCur = 5f;
    private float simulationSpeedVel = 0f;

    [Header("Burst Aiming")]
    public AnimationCurve MaxDistanceByChargeTime;

    public float AimTime;
    private Vector2 aimVectorCur;
    private Vector2 aimVectorVel;
    
    
    private void Start() {
        Controller.OnCollectPowerup += OnCollectPowerup;
        Controller.OnActionPressed += ActionPressed;
        Controller.OnActionReleased += ActionReleased;
    }

    public void SetRewiredPlayer(Player newPlayer) {
        rewiredPlayer = newPlayer;
    }
    
    private void ActionPressed() {
        if (hasPowerup) {
            isChargingUp = true;
        }
    }

    private void ActionHeld() {
        
    }

    private void ActionReleased() {
        if (!isChargingUp) return;
        isChargingUp = false;
        if (chargeTimer < TimerReleaseThreshold) {
            chargeTimer = 0f;
        }
        else {
            // Do FX Burst
            GameObject newBurst = Instantiate(BurstFX.gameObject, RingTransform.position, Quaternion.identity.normalized, null);
            newBurst.SetActive(true);
            newBurst.transform.localScale = Vector3.one * RingSizeByChargeTime.Evaluate(chargeTimer);
            newBurst.GetComponent<FxController>().SetUp(gameObject);
            newBurst.GetComponent<FxController>().TriggerTest = true;
            newBurst.GetComponent<FxController>().AutoKill = true;
            
            // Kill Enemies
            List<EnemyController> enemiesInRange = EnemyManager.Instance.GetEnemiesInRadius(RingTransform.position, RingSizeByChargeTime.Evaluate(chargeTimer));
            foreach (EnemyController enemy in enemiesInRange) {
                enemy.ReceiveVirusBlast();
            }
            
            // Reset Powerup
            hasPowerup = false;
            chargeTimer = 0f;
        }
    }

    private void Update() {
        if (isChargingUp) {  
            chargeTimer += Time.deltaTime;
            ringGradientTimer += Time.deltaTime * GradientSpeedByChargeTime.Evaluate(chargeTimer);
            RingSpriteRenderer.color = RingGradient.Evaluate(ringGradientTimer % 1f);
            BurstFX.transform.localScale = Vector3.one * RingSizeByChargeTime.Evaluate(chargeTimer);
            RingTransform.transform.localScale = Vector3.one * (RingSizeByChargeTime.Evaluate(chargeTimer) + Random.Range(-RingRandomnessByChargeTime.Evaluate(chargeTimer), RingRandomnessByChargeTime.Evaluate(chargeTimer)));
            ChargeUpFX.ToggleAll(true, gameObject, ChargeUpFXStrengthByChargeTime.Evaluate(chargeTimer));
            ChargeLoop.volume = LoopVolumeByChargeTime.Evaluate(chargeTimer);
            SoundManager.Instance.PlaySound(ChargeLoop, gameObject);
            Vector2 targetVector = new Vector2(rewiredPlayer.GetAxis("MoveX"), rewiredPlayer.GetAxis("MoveY")) * MaxDistanceByChargeTime.Evaluate(chargeTimer);
            aimVectorCur = Vector2.SmoothDamp(aimVectorCur, targetVector, ref aimVectorVel, AimTime);
        }
        else {
            RingTransform.transform.localScale = Vector3.zero;
            ChargeUpFX.ToggleAll(false);
            ChargeLoop.volume = 0f;
            aimVectorCur = aimVectorVel = Vector2.zero;
        }
        
        // Control Lightning Particles
        var backParticlesEmission = BackParticles.emission;
        backParticlesEmission.rateOverTime = hasPowerup ? BackParticlesEmitRate : 0f;
        var frontParticlesEmission = FrontParticles.emission;
        frontParticlesEmission.rateOverTime = hasPowerup ? FrontParticlesEmitRate : 0f;

        simulationSpeedCur = Mathf.SmoothDamp(simulationSpeedCur, hasPowerup ? OnSimulationSpeed : OffSimulationSpeed, ref simulationSpeedVel, SimulationSpeedTransitionTime);
        var backParticlesMain = BackParticles.main;
        backParticlesMain.simulationSpeed = simulationSpeedCur;
        var frontParticlesMain = FrontParticles.main;
        frontParticlesMain.simulationSpeed = simulationSpeedCur;
        
        // Move Ring
        RingTransform.localPosition = aimVectorCur;
        
    }

    public float GetMoveMultiplier() {
        if (isChargingUp) {
            return MoveMultiplierByChargeTime.Evaluate(chargeTimer);
        }

        if (hasPowerup) {
            return 0.85f;
        }
        return 1f;
    }

    private void OnCollectPowerup() {
        hasPowerup = true;
    }
}

public interface IMoveInfluence {

    float GetMoveMultiplier();

}

public interface IPlayerRecipient {

    void SetRewiredPlayer(Player newPlayer);

}
