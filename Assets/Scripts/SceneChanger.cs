﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger {

    public static void GoToMainMenu() {
        SceneManager.LoadScene("MainMuzel");
    }

    public static void GoToLevelOne() {
        SceneManager.LoadScene("Level 1");
    }
    
}
