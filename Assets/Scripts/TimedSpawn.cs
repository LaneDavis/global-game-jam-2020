﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedSpawn : MonoBehaviour
{
  private Collider2D disableSpawn;
  public GameObject enemy;
  public float spawnTime = 5f;
  public float spawnDelay = 3f;
  public float spawnTimeRandomnessPercent = 0.25f;
  public float xMin = 0;
  public float xMax = 5;
  public float yMin = 0;
  public float yMax = 5;
  public float moveMultiplier = 1.0f;
  private bool playerIsNearby = false;
  private float timeOfNextSpawn;

  // Use this for initialization
  void Start() {
    SetTimeOfNextSpawn(true);
  }

  private void OnTriggerStay2D(Collider2D other)
  {
    if (other.tag == "Player")
    {
      CancelInvoke("AddEnemy");
      playerIsNearby = true;
      //gameObject.SetActive(false);
    }
  }
  
  private void OnTriggerExit2D(Collider2D other)
  {
    if (other.tag == "Player")
    {
      InvokeRepeating("AddEnemy", spawnDelay, spawnTime);

//      CancelInvoke("AddEnemy");
  //    gameObject.SetActive(false);
    }
  }

  private void Update() {
    if (Time.time > timeOfNextSpawn && !playerIsNearby) {
      AddEnemy();
    }
  }

  private void SetTimeOfNextSpawn(bool addDelay) {
    timeOfNextSpawn = Time.time + (addDelay ? spawnDelay : 0f) + spawnTime * (1f + Random.Range(-spawnTimeRandomnessPercent, spawnTimeRandomnessPercent));
  }

  void AddEnemy() {
    SetTimeOfNextSpawn(false);
    // Instantiate a random enemy.
    Vector2 pos = new Vector2(Random.Range(xMin, xMax), Random.Range(yMin, yMax));
    GameObject newEnemy = Instantiate(enemy, pos, transform.rotation);
    EnemyController newController = newEnemy.GetComponent<EnemyController>();
    if (newController != null) {
      newController.moveSpeedMultiplier = moveMultiplier;
    }
  }
}
