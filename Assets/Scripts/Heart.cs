﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour {
    public Animator heartAnimation;
    public float maxAnimationSpeed = 5.0f;
    public float animationSpeedIncreasePerUpdate = .02f;

    private bool heartAnimationShouldIncrease = false;

    public FxController ExplosionFX;
    public SpriteRenderer HeartRenderer;

    public void ExplodeHeart() {
        heartAnimationShouldIncrease = true;
    }

    public void Update() {
        if (!heartAnimationShouldIncrease)
            return;
        if (heartAnimation.speed >= maxAnimationSpeed) {
            heartAnimationShouldIncrease = false;
            TriggerHeartExplosion();
            return;
        }

        heartAnimation.speed += animationSpeedIncreasePerUpdate;
    }

    private void TriggerHeartExplosion() {

        Debug.Log("Explode now");
        ExplosionFX.Trigger();
        ExplosionFX.ToggleAll(true);

        StartCoroutine("DeleteHeart");
        StartCoroutine("TransitionToMenu");
    }

    private IEnumerator DeleteHeart() {
        yield return new WaitForSeconds(.5f);
        HeartRenderer.color = Color.clear;
        // Destroy(gameObject);
    }

    private IEnumerator TransitionToMenu() {
        yield return new WaitForSeconds(6);
        SceneChanger.GoToMainMenu();
    }
}
