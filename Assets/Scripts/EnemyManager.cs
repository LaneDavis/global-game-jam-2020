﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyManager : MonoBehaviour {

    public static EnemyManager Instance;

    private List<EnemyController> activeEnemies = new List<EnemyController>();

    private void Awake() {
        Instance = this;
    }

    public void RegisterEnemy(EnemyController newEnemyController) {
        if (!activeEnemies.Contains(newEnemyController)) {
            activeEnemies.Add(newEnemyController);
        }
    }

    public void UnregisterEnemy(EnemyController oldEnemyController) {
        activeEnemies.Remove(oldEnemyController);
    }

    public List<EnemyController> GetEnemiesInRadius(Vector2 center, float radius) {
        return activeEnemies.Where(e => Vector2.Distance(e.transform.position, center) < radius).ToList();
    }

}
