﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector2Extensions {
    
    public static Vector2 Rotate (this Vector2 v, float degrees) {
        float ca = Mathf.Cos(degrees * Mathf.Deg2Rad);
        float sa = Mathf.Sin(degrees * Mathf.Deg2Rad);
        v = new Vector2(ca * v.x - sa * v.y, sa * v.x + ca * v.y);
        return v;
    }
    
}
