﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class OrbRotationController : MonoBehaviour {

    public float FacingAngles;
    private Vector2 lastPosition;

    public AnimationCurve RotateSpeedByAngles;

    private void Update() {
        float angles = Vector2.Angle(transform.right, Vector2.right);
        FacingAngles = angles * (MathUtilities.IsClockwise(transform.right, Vector2.right) ? -1f : 1f);

        Vector2 travelDif = (Vector2) transform.position - lastPosition;
        float travelMag = travelDif.magnitude;
        if (travelMag < 0.01f) return;

        float travelAngles = Vector2.Angle(travelDif, Vector2.right);
        travelAngles *= MathUtilities.IsClockwise(travelDif, Vector2.right) ? -1f : 1f;

        float difAngles = Vector2.Angle(transform.right, travelDif) * (MathUtilities.IsClockwise(transform.right, travelDif) ? -1f : 1f);
        transform.Rotate(0f, 0f, Mathf.Sign(difAngles) * Time.deltaTime * RotateSpeedByAngles.Evaluate(Mathf.Abs(difAngles)));
        
        lastPosition = transform.position;
    }

}
