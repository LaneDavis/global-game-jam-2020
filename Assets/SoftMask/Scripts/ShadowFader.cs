﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowFader : MonoBehaviour {

    public EnemyController EnemyController;
    public SFPolygon Shadowcaster;

    public float DeathTime;
    
    private float opacCur;
    private float opacVel;
    
    private float scaleCur = 1f;
    private float scaleVel;

    private bool isDying;
    private float deathTimer;
    
    private void Start() {
        EnemyController.OnBeforeDeath += StartDeathEvent;
        opacCur = Shadowcaster.opacity;
    }

    private void StartDeathEvent() {
        transform.parent = null;
        isDying = true;
    }

    private void Update() {
        if (!isDying) return;
        deathTimer += Time.deltaTime;
        opacCur = Mathf.SmoothDamp(opacCur, 0f, ref opacVel, DeathTime / 1.25f);
        scaleCur = Mathf.SmoothDamp(scaleCur, 0f, ref scaleVel, DeathTime / 1.25f);
        Shadowcaster.opacity = opacCur;
        transform.localScale = Vector3.one * scaleCur;
        if (deathTimer > DeathTime) {
            Destroy(gameObject);
        }
    }

}
