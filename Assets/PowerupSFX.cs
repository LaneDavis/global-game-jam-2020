﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupSFX : MonoBehaviour {

    public List<SpriteRenderer> SpriteRenderers = new List<SpriteRenderer>();
    public Gradient RainbowGradient;
    private float timer;
    public float ColorChangeRate;

    public float RotationRate;
    
    
    
    void Update() {
        timer += Time.deltaTime * ColorChangeRate;
        Color color = RainbowGradient.Evaluate(timer % 1);
        SpriteRenderers.ForEach(e => e.color = new Color(color.r, color.g, color.b, e.color.a));
        transform.Rotate(0f,0f,RotationRate * Time.deltaTime);
    }
}
